package mx.sabadell.n2.generator.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.sabadell.n2.generator.dto.Customer;
import mx.sabadell.n2.generator.service.DataGeneratorService;

/**
 * @author Lenin Meza ljmeza@indracompany.com
 *
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1/generate")
public class DataGeneratorController {
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DataGeneratorService dataGeneratorService;

	@RequestMapping(value = "/json", method = RequestMethod.GET)
	public ResponseEntity<?> generateDaily(@RequestParam(name = "records", required = true) int records)
			throws Exception {
		List<Customer> customers = dataGeneratorService.generateCustomers(records);

		return ResponseEntity.ok(customers);
	}

}
