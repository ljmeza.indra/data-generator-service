package mx.sabadell.n2.generator.dto;

import lombok.Data;

@Data
public class Customer {
	private String dataPhoneNumber;
	private String dataName;
	private String dataPaternalSurname;
	private String dataMaternalSurname;
	private String dataBirthDate;
	private String dataGender;
	private String dataCurp;
	private String dataEmail;
}
