/**
 * 
 */
package mx.sabadell.n2.generator.service;

import java.util.Date;
import java.util.List;

import mx.sabadell.n2.generator.dto.Customer;

/**
 * @author Lenin Meza ljmeza@indracompany.com
 *
 */
public interface DataGeneratorService {
	
	/**
	 * 
	 * @return
	 */
	public List<Customer> generateCustomers(int records);
	
	/**
	 * 
	 * @return
	 */
	public String generatePhone();

	/**
	 * 
	 * @return
	 */
	public String generateFirstName();

	/**
	 * 
	 * @return
	 */
	public String generatePaternalSurname();
	
	/**
	 * 
	 * @return
	 */
	public String generateMaternalSurname();
	
	/**
	 * 
	 * @return
	 */
	public String generateBirthDate();
	
	/**
	 * 
	 * @return
	 */
	public String generateGender();
	
	/**
	 * 
	 * @return
	 */
	public String generateCurp(Customer customer);
	
	/**
	 * 
	 * @return
	 */
	public String generateEmail();
}
