/**
 * 
 */
package mx.sabadell.n2.generator.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

import mx.sabadell.n2.generator.dto.Customer;

/**
 * @author Lenin Meza ljmeza@indracompany.com
 *
 */
@Service
public class DataGeneratorServiceImpl implements DataGeneratorService {

	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	private FakeValuesService fakeValuesService = new FakeValuesService(new Locale("en-GB"), new RandomService());
	private Faker faker = new Faker();
	private static Random random = new Random();
	private static char[] VOCALS = {'A', 'E', 'I', 'O', 'U'};

	@Override
	public List<Customer> generateCustomers(int records) {
		List<Customer> customers = new ArrayList<>();
		// Iterate N times
		IntStream.range(0, records).forEach(i -> {
			Customer customer = new Customer();
			customer.setDataPhoneNumber(this.generatePhone());
			customer.setDataName(this.generateFirstName());
			customer.setDataPaternalSurname(this.generatePaternalSurname());
			customer.setDataMaternalSurname(this.generateMaternalSurname());
			customer.setDataBirthDate(this.generateBirthDate());
			customer.setDataGender(this.generateGender());
			customer.setDataCurp(this.generateCurp(customer));
			customer.setDataEmail(this.generateEmail());
			customers.add(customer);
		});
		return customers;
	}

	@Override
	public String generatePhone() {
		return fakeValuesService.regexify("[0-9]{10}");
	}

	@Override
	public String generateFirstName() {
		return faker.name().firstName();
	}

	@Override
	public String generatePaternalSurname() {
		return faker.name().lastName();
	}

	@Override
	public String generateMaternalSurname() {
		return faker.name().lastName();
	}

	@Override
	public String generateBirthDate() {
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		Date birthDay = faker.date().birthday();
		return simpleDateFormat.format(birthDay);
	}

	@Override
	public String generateGender() {
		Random r = new Random();
		int randomNumber = r.nextInt(2);
		return (randomNumber == 1) ? "MALE" : "FEMALE";
	}

	@Override
	public String generateCurp(Customer customer) {
		StringBuilder curp = new StringBuilder();
		curp.append(customer.getDataPaternalSurname().charAt(0));
//        curp.append( primeraVocal( primerApellido.substring( 1 ) ) );
		curp.append(customer.getDataMaternalSurname().charAt(0));
		curp.append(customer.getDataName().charAt(0));
//        curp.append( fechaDeNacimiento.substring( 8, 10 ) );
//        curp.append( fechaDeNacimiento.substring( 0, 2 ) );
//        curp.append( fechaDeNacimiento.substring( 3, 5 ) );
//        curp.append( sexo );
//        curp.append( entidadDeNacimiento );
//        curp.append( primeraLetra( primerApellido.substring( 1 ) ) );
//        curp.append( primeraLetra( segundoApellido.substring( 1 ) ) );
//        curp.append( primeraLetra( nombres.substring( 1 ) ) );
        curp.append( 0 );
        curp.append( random.nextInt( 10 ) );
		return curp.toString();
	}

	@Override
	public String generateEmail() {
		return faker.internet().emailAddress();
	}
	
	private static char primeraLetra( String s )
    {
//        int i = StringUtils.indexOfAnyBut( s, VOCALS );
//        if ( i >= 0 )
//        {
//            return s.charAt( i );
//        }
        return 'X';
    }

    private static char primeraVocal( String s )
    {
//        int i = StringUtils.indexOfAny( s, VOCALS );
//        if ( i >= 0 )
//        {
//            return s.charAt( i );
//        }
        return 'X';
    }
}
