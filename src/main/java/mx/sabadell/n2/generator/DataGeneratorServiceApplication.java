package mx.sabadell.n2.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 
 * @author Lenin Meza ljmeza@indracompany.com
 *
 */
@SpringBootApplication
@ComponentScan("mx.sabadell.n2.generator")
public class DataGeneratorServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataGeneratorServiceApplication.class, args);
	}

}
